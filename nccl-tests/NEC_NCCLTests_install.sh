#!/bin/bash

module load modenv/hiera  GCCcore/11.3.0 GCC/11.3.0  OpenMPI/4.1.4  NCCL/2.15.5-CUDA-11.8.0

git clone --branch=v2.13.8 https://github.com/NVIDIA/nccl-tests.git
cd nccl-tests

make \
	MPI=1 MPI_HOME=/sw/modules/hiera/all/MPI/GCC/11.3.0/OpenMPI/4.1.4 \
	NCCL_HOME=/sw/installed/NCCL/2.15.5-GCCcore-11.3.0-CUDA-11.8.0 \
	CUDA_HOME=/sw/installed/CUDA/11.8.0/ 
	
# build in NGC
# make MPI=1 MPI_HOME=/opt/hpcx/ompi/
