#!/bin/bash

module load modenv/hiera  GCCcore/11.3.0 GCC/11.3.0  OpenMPI/4.1.4  NCCL/2.15.5-CUDA-11.8.0


i=$1
j=$2

hostname=`hostname`

if [[ $hostname == "taurusi8003" ]]; then
    A=$i
    export CUDA_VISIBLE_DEVICES=$A # A
elif [[ $hostname == "taurusi8010" ]]; then
    B=$j
    export CUDA_VISIBLE_DEVICES=$B # B
else
    echo "!!!!!!!!!!! unknown Host"
fi

echo `hostname` " with device: " $CUDA_VISIBLE_DEVICES
./c/mpi/pt2pt/standard/osu_bibw -d cuda D D
if [[ $hostname == "taurusi8003" ]]; then
    echo ""
    echo "--------------------------------------------------------------------------------------------------------------------------"
    echo ""
    echo ""
    echo ""
    echo ""
fi

