#!/bin/bash

module load modenv/hiera  GCCcore/11.3.0 GCC/11.3.0  OpenMPI/4.1.4  NCCL/2.15.5-CUDA-11.8.0

wget https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-7.3.tar.gz
tar xfz osu-micro-benchmarks-7.3.tar.gz
cd osu-micro-benchmarks-7.3
./configure \
	--enable-cuda --with-cuda=/sw/installed/CUDA/11.8.0 \
	CC=/sw/installed/OpenMPI/4.1.4-GCC-11.3.0/bin/mpicc \
	CXX=/sw/installed/OpenMPI/4.1.4-GCC-11.3.0/bin/mpicxx \
	NVCCFLAGS="-gencode=arch=compute_86,code=compute_86"


# for building in NGC
#./configure \
#	CC=/usr/local/mpi/bin/mpicc \
#	CXX=/usr/local/mpi/bin/mpicxx \
#	--enable-cuda --with-cuda=/usr/local/cuda \
#	--enable-ncclomb

make
